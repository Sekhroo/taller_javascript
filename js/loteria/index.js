//Variables base del juego
var intervalo_max = 100; // intervalo de números aleatorios entre 1 y intervalo_max
var num_aciertos = 0; // numero de aciertos realizados
var arr_num_user=[]; // inicializar array de los números del usuario
var arr_num_rnd=[]; // inicializar array de los números randoms
var arr_aciertos=[]; // inicializar array de los aciertos conseguidos

// Función para obtener número aleatorio
var rnd=function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max))+1;
}

// Comprobación entre número obtenido del usuario y el random
function generar_loteria() {
    var n_rnd;
    for (var fin = 0; fin < 100000; fin++) {
        arr_num_rnd = [];
        for (var i = 0; i < 6; i++) {
            // números entre 1 y X; si rnd(X)
            n_rnd = rnd(intervalo_max);

            // añadirla a un array de aleatorios
            arr_num_rnd.push(n_rnd);
        }
        
        // Comprobar si existen repetidos
        const comprobar_rnd_unicos = [...new Set(arr_num_rnd)]; // Array sin duplicados
        let no_unicos = [...arr_num_rnd]; // Creamos una copia del array original
        comprobar_rnd_unicos.forEach((numero) => {
            const indice = no_unicos.indexOf(numero);
            no_unicos = no_unicos.slice(0, indice)
            .concat(no_unicos.slice(indice + 1, no_unicos.length));
        });

        if (no_unicos.length == 0) {
            fin = 100000;
            return
        }
    }
}

function imprimir_pantalla_rnd() {
    document.querySelector('.loteria_rnd_1').textContent = arr_num_rnd[0];
    document.querySelector('.loteria_rnd_2').textContent = arr_num_rnd[1];
    document.querySelector('.loteria_rnd_3').textContent = arr_num_rnd[2];
    document.querySelector('.loteria_rnd_4').textContent = arr_num_rnd[3];
    document.querySelector('.loteria_rnd_5').textContent = arr_num_rnd[4];
    document.querySelector('.loteria_rnd_6').textContent = arr_num_rnd[5];
}

// Comprobación entre número obtenido del usuario y el random
function comprobar(num_random, num_usuario) {
    // Generar un acierto
    if(num_random == num_usuario) {
        arr_aciertos.push(num_usuario);
    }
}

// obtener aciertos
function obtener_aciertos(){
    var compro = 0;
    for (var i_rnd = 0; i_rnd < 6; i_rnd++) {
        for (var i_user = 0; i_user < 6; i_user++) {
            comprobar(arr_num_rnd[i_rnd], arr_num_user[i_user]);  
        }
    }
}


// imprimir aciertos
function imprimir_aciertos(){
    document.querySelector('.num_acertados').textContent = arr_aciertos.join("-");
}

// Actualizar resultados en la página
function resultados_juego() {
    if (arr_aciertos.length == 6) {
        document.querySelector('.mng_loteria').textContent = '!!Ganaste¡¡';
    }else if (arr_aciertos.length >= 4) {
        document.querySelector('.mng_loteria').textContent = '¡¡Casi!!';
    }else {
        document.querySelector('.mng_loteria').textContent = '¡¡Inténtalo!!';
    }
}

//victoria
function victoria() {
    if (num_aciertos==6) return false;
    else {
        return true;
    }
}


//Botón principal de adivinanza: verificar
document.querySelector('.loteria_reiniciar').addEventListener('click', function(){
    if ((victoria())) {
        arr_num_rnd = [];

        var user_num1 = document.querySelector('.loteria_user_1').value;
        var user_num2 = document.querySelector('.loteria_user_2').value;
        var user_num3 = document.querySelector('.loteria_user_3').value;
        var user_num4 = document.querySelector('.loteria_user_4').value;
        var user_num5 = document.querySelector('.loteria_user_5').value;
        var user_num6 = document.querySelector('.loteria_user_6').value;
        arr_num_user.push(user_num1);
        arr_num_user.push(user_num2);
        arr_num_user.push(user_num3);
        arr_num_user.push(user_num4);
        arr_num_user.push(user_num5);
        arr_num_user.push(user_num6);
    
        generar_loteria();
        imprimir_pantalla_rnd();
        obtener_aciertos();
        imprimir_aciertos();
        resultados_juego();

        arr_num_user = [];
        arr_aciertos = [];
        arr_num_rnd = [];
    }else {
        document.querySelector('.loteria_reiniciar').disabled=true;
    }
});

//Botón principal de adivinanza: reinicio
document.querySelector('.loteria_ok').addEventListener('click', function(){
    document.querySelector('.loteria_reiniciar').disabled=false;
});