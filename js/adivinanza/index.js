//Variables base del juego
var intervalo_max=100; // intervalo de números aleatorios entre 1 y intervalo_max
var vida=7; // cantidad de intentos del usuario
var arr=[]; // inicializar array del maxpuntaje conseguido

// Función para obtener número aleatorio
var rnd=function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max))+1;
}

// número entre 1 y X; si rnd(X)
var num_random = rnd(intervalo_max);

// Comprobación entre número obtenido del usuario y el random
function comprobar_adivinanza(num_random, num_usuario) {
    // número de usuario es mayor al generado
    if(num_usuario > num_random) {
        vida = vida-1;
        adivinanza_resultados_juego('Demaciado alto!!', vida);


    // número generado es mayor al del usuario
    }else if(num_usuario < num_random) {
        vida = vida-1;
        adivinanza_resultados_juego('Demaciado bajo!!', vida);


    // número de usuario y generado son idénticos
    }else {
        adivinanza_resultados_juego('Ganaste, número correcto',vida)
        try_max_pts(vida);
        document.querySelector('.adivinanza_verificar').disabled=true;
        
    }
}

// Conseguir puntaje máximo conseguido
function try_max_pts(pts){
    arr.push(pts);
    var max_pts = Math.max.apply(Math,arr);
    document.querySelector('.max_puntaje').textContent = max_pts;
}

// Actualizar resultados en la página
function adivinanza_resultados_juego(respuesta, vida) {
    document.querySelector('.mensaje').textContent = respuesta;
    document.querySelector('.vida').textContent = vida;
}

// Inicia el juego nuevamente
function start() {
    document.querySelector('.adivinanza_verificar').disabled=false;
    document.querySelector('.mensaje').textContent = '?';
    vida=7;
    document.querySelector('.vida').textContent = vida;
    num_random = rnd(intervalo_max);
}

function end_game() {
    if (vida>1) return false;
    else {
        return true;
    }
}

//Botón principal de adivinanza: verificar
document.querySelector('.adivinanza_verificar').addEventListener('click', function(){
    if (!(end_game())) {
        var user_num = document.querySelector('.adivina_user_num').value;
        comprobar_adivinanza(num_random, user_num);
    }else {
        adivinanza_resultados_juego('Perdiste, ¡suerte al reiniciar!', vida-1);
        document.querySelector('.adivinanza_verificar').disabled=true;
    }
});

//Botón principal de adivinanza: reinicio
document.querySelector('.adivinanza_reiniciar').addEventListener('click', function(){
    start();
});