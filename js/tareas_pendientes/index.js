const input = document.querySelector(".tareas_input");
const addBtn = document.querySelector(".tareas_btn-add");
const ul = document.querySelector(".tareas_ul");
const empty = document.querySelector(".tareas_empty");

addBtn.addEventListener("click", (e) => {
  e.preventDefault();

  const text = input.value;

  if (text !== "") {
    const li = document.createElement(".tareas_li");
    const p = document.createElement(".tareas_p");
    p.textContent = text;

    li.appendChild(p);
    li.appendChild(addDeleteBtn());
    ul.appendChild(li);

    input.value = "";
    empty.style.display = "none";
  }
});

function addDeleteBtn() {
  const deleteBtn = document.createElement(".tareas_button");

  deleteBtn.textContent = "X";
  deleteBtn.className = ".tareas_btn-delete";

  deleteBtn.addEventListener("click", (e) => {
    const item = e.target.parentElement;
    ul.removeChild(item);

    const items = document.querySelectorAll(".tareas_li");

    if (items.length === 0) {
      empty.style.display = "block";
    }
  });

  return deleteBtn;
}